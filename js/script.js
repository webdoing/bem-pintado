var slideIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("container-carousel");
    for (i = 0; i < x.length; i++) {
      //x[i].style.display = "none";
      x[i].style.opacity = ".9";
      x[i].style.visibility = "hidden";
    }
    slideIndex++;
    if (slideIndex > x.length) {
      slideIndex = 1
    }
    //x[slideIndex-1].style.display = "block";
    x[slideIndex-1].style.opacity = "1";
    x[slideIndex-1].style.visibility = "visible";
    timer = setTimeout(carousel, 3000); // Change image every 2 seconds
}

showDivs(slideIndex);

// troca de imagem no carousel
function plusDivs(n) {
    // reseto o timeout do carousel
    clearTimeout(timer)
    // seto novamente o timeout do carousel
    timer = setTimeout(carousel, 3000)
    // chamando a função de trocar a imagem
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("container-carousel");
    if (n > x.length) {
      slideIndex = 1
    }
    if (n < 1) {
      slideIndex = x.length
    } ;
    for (i = 0; i < x.length; i++) {
        //x[i].style.display = "none";
        x[i].style.opacity = ".5";
        x[i].style.visibility = "hidden";
    }
    //x[slideIndex-1].style.display = "block";
    x[slideIndex-1].style.opacity = "1";
    x[slideIndex-1].style.visibility = "visible";
}
